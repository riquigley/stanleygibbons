<?php
/**
 * Class: Calculator
 *
 * @copyright 2015 Ike Quigley
 * @author Ike Quigley <ikequigley@gmail.com>
 */

/*
 * Main processor for calculating the Mean, Mode, Median, and Range.
 */

class Calculator
{
   /*
    * Calculates the average of all the numbers.  O(N) complexity.
    * @param $numbers Array of input values
    * @return float The average of the values, or null for no average
    */
   public static function getMean($numbers)
   {
      $sum = 0;
      $count = 0;
      if (!is_array($numbers)) {
          return null;
      }
      foreach ($numbers as $num) { // O(N)
         $sum+= $num;
         $count++;
      }
      if ($count == 0) {
         return null;
      }
      return round($sum / $count, 3);
   }

   /*
    * Calculates the middle values of the numbers.  O(NlogN) on average with
    * rare worst case of O(N^2).
    * @param $numbers Array of input values
    * @return real The middle of the values, or null if there is not one.
    */
   public static function getMedian($numbers)
   {
      // Sort the numbers.
      if (!is_array($numbers)) {
          return null;
      }
      $count = count($numbers);  // O(1)
      if ($count < 2 ) {
          return null;
      }
      sort($numbers); // O(NlogN) on average
      if ($count % 2 == 0) {
         // We have an even number of values
         $median1 = $numbers[$count / 2 - 1];
         $median2 = $numbers[$count / 2];
         // Return the average of the two values
         return round(($median1 + $median2) / 2, 3);
      }
      else {
         // Just return the middle value
         return $numbers[ floor( $count / 2) ];
      }
   }

   /*
    * Calculates the most common value. O(N).
    * @param $numbers Array of input values
    * @return Real. The most frequently found value, or a comma delimited string
    * of values for multiple modes, or null of there isn't a mode.
    */
   public static function getMode($numbers)
   {
      // Use this array to track how many of each number we have.
      $countArray = array();

      // Check for an empty list
      if (!count($numbers) > 0) {
         return null;
      }
      // Iterate all and count occurence of each
      foreach ($numbers as $num) {  // O(N)
         if (array_key_exists($num, $countArray)) { // O(1)
            $countArray[$num] ++;
         }
         else {
            $countArray[$num] = 1;
         }
      }
      // Now find the highest
      $highestCount = 1;
      // We can have multiple modes, so track it here.
      $highestNumber = array();

      foreach ($countArray as $key => $value) { // O(N)
         // Set to new highest if there is one
         if ($value > $highestCount) {
            $highestCount = $value;
            // Reset the array of modes.
            $highestNumber = array();
            array_push($highestNumber, $key);
         }
         // Or add another if there is a tie.
         else if ($value == $highestCount && $value != 1) {
            array_push($highestNumber, $key);
         }
      }
      if (!empty($highestNumber)) {
         return implode(",", $highestNumber);
      }
      return null;
   }

   /*
    * Calculates the range of values.  Iterates through all values in O(N) time
    * without having to sort the array, which would result in O(NlogN) complexity.
    * @param $numbers : Array of input values
    * @return int: The middle of the values, or null if there is not one.
    */
   public static function getRange($numbers)
   {
      // Check for an empty set or set of 1
      if (count($numbers) < 2) {
         return null;
      }
      // Initialize high and low
      $high = $numbers[0];
      $low = $numbers[0];
      // Iterate through to find new highs and lows
      foreach ($numbers as $num) {
         if ($num > $high) {
            $high = $num;
         }
         if ($num < $low) {
            $low = $num;
         }
      }
      // Return the range.
      return $high - $low;
   }
}
