<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CalculatorTest
 *
 * @author ike
 */
require_once __DIR__ . '/../Calculator.php';

class CalculatorTest extends PHPUnit_Framework_TestCase {

    /**
     * @dataProvider numberProvider
     */
    public function testMean($mean,$median,$mode,$range,$numbers) {
        $this->assertEquals($mean, Calculator::getMean($numbers));
    }

    /**
     * @dataProvider numberProvider
     */
    public function testMode($mean,$median,$mode,$range,$numbers) {
        $this->assertEquals($mode, Calculator::getMode($numbers));
    }

    /**
     * @dataProvider numberProvider
     */    
    public function testMedian($mean,$median,$mode,$range,$numbers) {
        $this->assertEquals($median, Calculator::getMedian($numbers));
    }

    /**
     * @dataProvider numberProvider
     */       
    public function testRange($mean,$median,$mode,$range,$numbers) {
        $this->assertEquals($range, Calculator::getRange($numbers));
    }
    
    public function numberProvider() {
        return array (
            // Given Sample Data
            array('mean'=> 6.2, 'median'=> 6, 'mode'=>5, 'range'=> 3, 'numbers'=> array(5, 6, 8, 7, 5) ),
            // Simple Input
            array('mean'=> 15, 'median'=> 14, 'mode'=>13, 'range'=> 8, 'numbers'=> array(13, 18, 13, 14, 13, 16, 14, 21, 13) ),
            // Single Input
            array('mean'=> 8, 'median'=> null, 'mode'=> null, 'range'=> 0, 'numbers'=> array (8) ),
            // Zero Input
            array('mean'=> 0, 'median'=> null, 'mode'=> null, 'range'=> 0, 'numbers'=> array(0) ),
            // Null Input
            array('mean'=> null, 'median'=> null, 'mode'=> null, 'range'=> null, 'numbers'=> null ),
            // with No Mode
            array('mean'=> 3.5, 'median'=> 3, 'mode'=> null, 'range'=> 6, 'numbers'=> array(1, 2, 4, 7) ),
            // with Zero Range
            array('mean'=> 4, 'median'=> 4, 'mode'=>4, 'range'=> 0, 'numbers'=> array(4, 4, 4, 4, 4) ),
            // with Zero Average
            array('mean'=> 0, 'median'=> 0, 'mode'=>'-3,3', 'range'=> 6, 'numbers'=> array(-3, -3, -3, 3, 3, 3) ),
            // With Multiple Modes and Split Median
            array('mean'=> 10.5, 'median'=> 10.5, 'mode'=> '10,11', 'range'=> 5, 'numbers'=> array(8, 9, 10, 10, 10, 11, 11, 11, 12, 13) ),
            // With Negatives
            array('mean'=> 7.222, 'median'=> 13, 'mode'=>13, 'range'=> 39, 'numbers'=> array(13, 18, 13, -14, 13, 16, 14, -21, 13) ),
            // All Negatives
            array('mean'=> -6.2, 'median'=> -6, 'mode'=>-5, 'range'=> 3, 'numbers'=> array(-5, -6, -8, -7, -5) ),
        );
    }

}
