<?php
/**
 * Class: Error
 *
 * @copyright 2015 Ike Quigley
 * @author Ike Quigley <ikequigley@gmail.com>
 */

/*
 * The Error class logs and returns errors in JSON format.
 */
class Error
{
   private $flag = false;
   private $code  = 0;
   private $message = "";


   /*
    * Receives an error code and message.  Sets the error flag to true.
    * @param $_code Integer the error code
    * @param $_message String the message attached to this error.
    */
   public function setError($_code, $_message) {
      $this->code = $_code;
      $this->message = $_message;
      $this->flag = true;
   }

   /*
    * @return boolean to signify that there is an error to return.
    */
   public function hasError() {
      return $this->flag;
   }

   /*
    * @return JSON encoded version of this error object.
    */
   public function getError() {
      $error = array();
      $error['error'] = array();
      $error['error']['code'] = $this->code;
      $error['error']['message'] = $this->message;
      return json_encode($error);
   }

}
