<?php
/**
 * Class: Processor
 *
 * @copyright 2015 Ike Quigley
 * @author Ike Quigley <ikequigley@gmail.com>
 */

/*
 * The Processor Class Receives the incoming JSON object, decodes it, calls
 * upon the NumberProcessor object to retrieve the mean, median, mode, and range.
 * Then formats those answers into the specified return JSON object. 
 */
require_once("Error.php");
require_once('Calculator.php');

class Processor
{
   /* Standard Object converted from JSON string */
   private $input;
   /* NumberTree object for advanced processing */
   private $numberTree;
   /* Error object for processing errors */
   private $error;

   // For storing our answers.
   private $median;
   private $mode;
   private $range;
   private $mean;

   public function __construct() {
      $this->error = new Error();
   }

   /*
    * Receives the incoming JSON object.
    * @param $_input JSON Obj
    * @return Boolean. False if error detected for early exit, True otherwise.
    */
   public function processInput($_input) {
      $this->input = json_decode($_input);

      // Check for a well-formed JSON input string
      if ($this->input == NULL || $this->input == false) {
         $this->error->setError(500, "Malformed JSON input string.");
         return false;
      }
      // Check that we have a numbers property
      if (!property_exists($this->input, 'numbers')){
         $this->error->setError(500, "JSON _numbers_ property not found");
         return false;
      }
      // Check that the numbers property has numbers in it.
      if ( empty($this->input->numbers) ) {
         $this->error->setError(500, "No numbers found in JSON request");
         return false;
      }
      $this->calcAnswers();

      return true;
   }

   /*
    * Calls on the Calculator Static class functions to retrieve each answer.
    */
   private function calcAnswers(){
      $this->median = Calculator::getMedian($this->input->numbers);
      $this->mode = Calculator::getMode($this->input->numbers);
      $this->range = Calculator::getRange($this->input->numbers);
      $this->mean = Calculator::getMean($this->input->numbers);
   }


   /*
    * Formats the answers into a well-formed JSON Obj.
    * @returns JSON String
    */
   public function getAnswer(){
      // Check to see if there was an error with the process
      if ($this->error->hasError()){
         return $this->error->getError();
      }
      // Else everything processed find, let's return the answer in json format.
      $tempObj = new stdClass();
      $tempObj->results = array('mean'=>$this->mean,
                                'median'=>$this->median,
                                'mode'=>$this->mode,
                                'range'=>$this->range);

      return json_encode($tempObj);
   }

}
