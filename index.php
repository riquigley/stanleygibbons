<?php
   require_once("Error.php");
   require_once("Processor.php");

   // For processing errors.
   $error = new Error();
   // Placeholder for returning our response
   $response;

   // For checking our request type and route.
   $requestType = $_SERVER['REQUEST_METHOD'];
   $requestRoute = $_SERVER['REQUEST_URI'];

   /* -- This block acts as our routing control class. -- */

   // Grab our parameters.
   $parameters = explode("/",$requestRoute);
   // For this practice, we'll assume the route is always the last parameter.
   $route = $parameters[ count($parameters) - 1 ];

   // Now check the route
   if ($route == "mmmr") {
      // Now make sure that the request is a post request.
      if ($requestType != "POST"){
         $error->setError(404, "Method {$requestType} not available on this endpoint");
         $response = $error->getError();
      // Make sure that the numbers parameter is set.
      } elseif (!isset($_POST['numbers'])) {
         $error->setError(404, "_numbers_ parameter not found.");
         $response = $error->getError();
      // Everything seems ok, so we'll try to process the input.
      } else {
         $processor = new Processor();
         $processor->processInput($_POST['numbers']);
         $response = $processor->getAnswer();
      }
   // Else the route is not correct.
   } else {
      $error->setError(404,"Requested Resource Not Found");
      $response = $error->getError();
   }

   // Return our response
   echo $response;
?>